return {
  'tadmccorkle/markdown.nvim',
  ft = 'markdown',
  opts = {
    mappings = {
      -- the gx keymap works out of the box on all file type.
      -- Setting it in this plugin seems useless and in my case is broken in Neovide.
      -- less is more.
      link_follow = false, -- (string|boolean) follow link
    },
  },
}
