return {
  'L3MON4D3/LuaSnip',
  version = 'v2.*',
  dependencies = {
    'benfowler/telescope-luasnip.nvim',
    'rafamadriz/friendly-snippets',
  },
  config = function()
    require('luasnip.loaders.from_vscode').lazy_load()
    require('luasnip.loaders.from_vscode').lazy_load { paths = { './my-snippets' } }

    local ls = require 'luasnip'
    ls.config.set_config {
      history = true,
      updateevents = 'TextChanged,TextChangedI',
      enable_autosnippets = true,
    }

    vim.keymap.set({ 'i' }, '<C-K>', function()
      ls.expand()
    end, { silent = true })
    vim.keymap.set({ 'i', 's' }, '<C-L>', function()
      ls.jump(1)
    end, { silent = true })
    vim.keymap.set({ 'i', 's' }, '<C-J>', function()
      ls.jump(-1)
    end, { silent = true })

    vim.keymap.set({ 'i', 's' }, '<C-E>', function()
      if ls.choice_active() then
        ls.change_choice(1)
      end
    end, { silent = true })
    local commander = require 'commander'
    local category = 'Format'
    commander.add {
      {
        desc = 'Find snippets',
        cmd = '<cmd>Telescope luasnip<CR>',
        keys = { { 'n', 'v' }, '<leader>ls', { noremap = true } },
        category = category,
      },
    }
  end,
}
