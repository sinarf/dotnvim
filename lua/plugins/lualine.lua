local function neovide()
  return 'Neovide: ' .. vim.g.neovide_version
end

local function venv()
  local venv_name = require('venv-selector').get_active_venv()
  vim.notify(venv_name)
  if venv_name ~= nil then
    return 'no venv'
  else
    return venv_name
  end
end

local custom_theme = require 'lualine.themes.tokyonight'
custom_theme.normal.c.bg = 'None'

return {
  'nvim-lualine/lualine.nvim',
  opts = {
    options = {
      theme = custom_theme,
      component_separators = {
        left = '',
        right = '',
      },
      section_separators = {
        left = '',
        right = '',
      },
    },
    sections = {
      lualine_a = {
        {
          'mode',
          separator = {
            left = '',
            right = '',
          },
          right_padding = 2,
        },
      },
      lualine_b = {
        'filename',
        'branch',
        'diagnostics',
      },
      lualine_c = {
        'fileformat',
      },
      lualine_m = {},
      lualine_x = {},
      lualine_y = {
        neovide,
        venv,
        'filetype',
        {
          require('lazy.status').updates,
          cond = require('lazy.status').has_updates,
          color = { fg = '#ff9e64' },
          separator = {
            left = '',
          },
          on_click = function()
            vim.api.nvim_command 'Lazy'
          end,
        },
      },
      lualine_z = {
        {
          'location',
          separator = {
            left = '',
            right = '',
          },
          left_padding = 2,
        },
      },
    },
    extensions = {
      'aerial',
      'mason',
      'oil',
      'quickfix',
      'trouble',
    },
  },
}
