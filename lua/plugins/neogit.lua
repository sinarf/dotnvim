return {
  'NeogitOrg/neogit',
  dependencies = {
    'nvim-lua/plenary.nvim', -- required
    'nvim-telescope/telescope.nvim', -- optional
    'sindrets/diffview.nvim', -- optional
  },
  opts = {
    auto_refresh = true,
    disable_hint = true,
    git_services = {
      ['github.com'] = 'https://github.com/${owner}/${repository}/compare/${branch_name}?expand=1',
      ['bitbucket.org'] = 'https://bitbucket.org/${owner}/${repository}/pull-requests/new?source=${branch_name}&t=1',
      ['gitlab.com'] = 'https://gitlab.com/${owner}/${repository}/merge_requests/new?merge_request[source_branch]=${branch_name}',
      ['framagit.org'] = 'https://framagit.org/${owner}/${repository}/merge_requests/new?merge_request[source_branch]=${branch_name}',
    },
    filewatcher = {
      interval = 100,
      enabled = true,
    },
    graph_style = 'unicode',
    integrations = {
      diffview = true,
      telescope = true,
    },
    kind = 'tab',
    mappings = {
      popup = {
        -- Default is 'l', I love the plugin but I cannot understand how overriding l would be a none hateful choice.
        ['o'] = 'LogPopup',
      },
    },
    sections = {
      recent = {
        folded = false,
        hidden = false,
      },
    },
    signs = {
      item = {
        '',
        '',
      },
      section = {
        '',
        '',
      },
    },
    status = {
      recent_commit_count = 40,
    },
  },
}
