return {
    'akinsho/toggleterm.nvim',
    version = "*",
    config = function()
        require("toggleterm").setup(
            {
                shade_terminals = false,
                open_mapping = [[<c-\>]],
                highlights = {
                    -- NormalFloat = {
                    --     link = 'Normal'
                    -- },
                    -- FloatBorder = {
                    --     guifg = "<VALUE-HERE>",
                    --     guibg = "<VALUE-HERE>",
                    -- },
                },
                -- shading_factor = 90,
                direction = "float",
                float_opts = {
                    border = "none",
                    winblend = 1,  -- minimal transparency for readability
                },
            }
        )
        local group = vim.api.nvim_create_augroup("term_autocmd", { clear = true })
        vim.api.nvim_create_autocmd("TermClose", {
            group = group,
            callback = function()
                vim.cmd("sleep 2| bdelete")
            end
        })
    end
}
