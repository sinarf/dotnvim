local opts = { noremap = true, silent = true }

vim.keymap.set('n', '<localleader>ac', ':! aerospace reload-config<CR>', { desc = 'Reload AeroSpace Configuration', unpack(opts) })
-- vim.keymap.set('n', '<localleader>al', '<cmd>split | terminal aerospace_app_id.sh <CR>' , { desc = 'Retrieve AeroSpace App id', unpack(opts) })
