local opts = { noremap = true, silent = true }

vim.keymap.set('n', '<localleader>f', '<cmd>! npm-groovy-lint --format %<CR>', { desc = 'Format with npm-groovy-lint', unpack(opts) })
