local opts = { noremap = true, silent = true }

-- vim.keymap.set('n', 'bd', ':split | resize 5 | terminal escrow/doc/generate_doc.sh -f % ; sleep 5<CR>', { desc = 'Build this asciidoc', unpack(opts) })
vim.keymap.set('n', '<localleader>bh', ':split | resize 5 | terminal asciidoctor --verbose % -o /tmp/asciidoc.nvim.html ; open /tmp/asciidoc.nvim.html<CR>', { desc = 'Build this asciidoc to html', unpack(opts) })
vim.keymap.set('n', '<localleader>bp', ':split | resize 5 | terminal asciidoctor-pdf --verbose % -o /tmp/asciidoc.nvim.pdf ; zathura /tmp/asciidoc.nvim.pdf<CR>', { desc = 'Build this asciidoc to PDF', unpack(opts) })

vim.keymap.set('n', '<localleader>p', '<cmd>AsciiDocPreview<CR>', { desc = 'AsciiDoc Preview', unpack(opts) })
