-- neovide configuration
-- Details https://github.com/neovide/neovide/wiki/Configuration
if vim.g.neovide then
  vim.g.neovide_cursor_antialiasing = true
  vim.g.neovide_cursor_vfx_mode = 'railgun'
  vim.g.neovide_cursor_vfx_particle_curl = 1.0
  vim.g.neovide_cursor_vfx_particle_density = 7.0
  vim.g.neovide_cursor_vfx_particle_lifetime = 3
  vim.g.neovide_cursor_vfx_particle_speed = 10.0
  vim.g.neovide_floating_blur_amount_x = 4.0
  vim.g.neovide_floating_blur_amount_y = 4.0
  vim.g.neovide_floating_shadow = true
  vim.g.neovide_floating_z_height = 10
  vim.g.neovide_light_angle_degrees = 45
  vim.g.neovide_light_radius = 5
  vim.g.neovide_padding_bottom = 10
  vim.g.neovide_padding_left = 10
  vim.g.neovide_padding_right = 10
  vim.g.neovide_padding_top = 0
  vim.g.neovide_transparency = 0.75
  vim.g.neovide_window_blurred = true
end

-- Handle the zoom, as if you were in a lesser editor that has those futile, yet handy, feature.
-- sourced from: https://github.com/neovide/neovide/issues/1301#issuecomment-1119370546
local font_size = 10
if vim.loop.os_uname().sysname == 'Darwin' then
  font_size = 15
end

vim.g.gui_font_default_size = font_size
vim.g.gui_font_size = vim.g.gui_font_default_size
vim.g.gui_font_face = 'Cascadia Code'
-- vim.g.gui_font_face = 'CommitMono Nerd Font'
RefreshGuiFont = function()
  vim.opt.guifont = string.format('%s:h%s', vim.g.gui_font_face, vim.g.gui_font_size)
end

ResizeGuiFont = function(delta)
  vim.g.gui_font_size = vim.g.gui_font_size + delta
  RefreshGuiFont()
end

ResetGuiFont = function()
  vim.g.gui_font_size = vim.g.gui_font_default_size
  RefreshGuiFont()
end

-- Call function on startup to set default value
ResetGuiFont()

-- Keymaps
local opts = { noremap = true, silent = true }
vim.keymap.set({ 'n', 'i' }, '<C-=>', function()
  ResizeGuiFont(1)
end, opts)
vim.keymap.set({ 'n', 'i' }, '<C-->', function()
  ResizeGuiFont(-1)
end, opts)
vim.keymap.set({ 'n', 'i' }, '<C-BS>', function()
  ResetGuiFont()
end, opts)
vim.keymap.set({ 'n', 'i' }, '<C-ScrollWheelUp>', function()
  ResizeGuiFont(1)
end, opts)
vim.keymap.set({ 'n', 'i' }, '<C-ScrollWheelDown>', function()
  ResizeGuiFont(-1)
end, opts)
vim.keymap.set({ 'n' }, '<leader>nf', function()
  vim.g.neovide_fullscreen = true
end, opts)
vim.keymap.set({ 'n' }, '<leader>nF', function()
  vim.g.neovide_fullscreen = false
end, opts)
