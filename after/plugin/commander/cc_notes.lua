local commander = require("commander")
local noremap = { noremap = true }
local category = 'notes'

commander.add({
  {
    desc = "Open Today Note",
    cmd = "<CMD>e $TODAY_NOTE<CR>",
    keys = { "n", "<leader>nt", noremap },
    category = category,
  },
  {
    desc = "Open WIP Note",
    cmd = "<CMD>e $WIP_NOTE<CR>",
    keys = { "n", "<leader>nw", noremap },
    category = category,
  },
  {
    desc = "Open Errand list",
    cmd = "<CMD>e $HOME/Sync/shop<CR>",
    keys = { "n", "<leader>ns", noremap },
    category = category,
  },
})
