local commander = require("commander")
local noremap = { noremap = true }

local category = 'scratchbook'

commander.add({
  {
    desc = "scratchbook bash",
    cmd = "<cmd>e ~/Sync/config/vim/scratchbook/scratchbook.sh<cr>",
    category = category,
  },
  {
    desc = "scratchbook python",
    cmd = "<cmd>e ~/Sync/config/vim/scratchbook/scratchbook.py<cr>",
    category = category,
  },
  {
    desc = "scratchbook groovy",
    cmd = "<cmd>e ~/Sync/config/vim/scratchbook/scratchbook.groovy<cr>",
    category = category,
  },
})

