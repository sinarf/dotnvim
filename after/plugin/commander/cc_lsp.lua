local commander = require("commander")
local silent_noremap = { noremap = true, silent = true }

local category = 'LSP'

commander.add({
  {
    desc = "Show function signaure (hover)",
    cmd = "<CMD>lua vim.lsp.buf.hover()<CR>",
    keys = {
      { "n", "K",     silent_noremap },
      { "i", "<C-k>", silent_noremap },
    },
    category = category,
  },
})
