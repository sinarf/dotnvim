local commander = require("commander")
local noremap = { noremap = true }

local category = 'buffer'


commander.add({
  {
    desc = "Close buffer",
    cmd = "<cmd>BD<CR>",
    category = category,
    keys = { "n", "<leader>bd", noremap },
  },
  {
    desc = "Previous buffer",
    cmd = "<cmd>bp<CR>",
    category = category,
    keys = { "n", "<leader>bp", noremap },
  },
  {
    desc = "Next buffer",
    cmd = "<cmd>bn<CR>",
    category = category,
    keys = { "n", "<leader>bn", noremap },
  },
  {
    desc = "Workspace cleanup",
    cmd = "<cmd>%bdelete | Alpha<cr>",
    keys = { "n", "<leader>xx", noremap },
    category = category,
  },
})
