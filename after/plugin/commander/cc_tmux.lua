local commander = require("commander")
local noremap = { noremap = true }

local category = 'tmux'


commander.add({
  {
    desc = "Open split tmux terminal",
    cmd = "<cmd>! tmux split-window -c .<CR><CR>",
    category = category,
    keys = { "n", "<leader>tv", noremap },
  },
  {
    desc = "Open horizontal split tmux terminal",
    cmd = "<cmd>! tmux split-window -h -c .<CR><CR>",
    category = category,
    keys = { "n", "<leader>th", noremap },
  },
})
